package com.gildedrose;

import java.util.ArrayList;
import java.util.List;

public class GildedRoseFixture {

  private final GildedRose gildedRose;
  private final List<ItemGildedRose> items;

  private GildedRoseFixture(List<ItemGildedRose> items) {
    this.items = items;
    gildedRose = new GildedRose(items);
  }

  void updateQuality() {
    gildedRose.updateQuality();
  }

  ItemGildedRose getFirstItem() {
    return items.get(0);
  }

  static class Builder {

    private final List<ItemGildedRose> items = new ArrayList<>();

    static Builder aGildedRose() {
      return new Builder();
    }

    Builder with(ItemFixture.Builder item) {
      items.add(item.build());
      return this;
    }

    GildedRoseFixture build() {
      return new GildedRoseFixture(items);
    }
  }
}
