package com.gildedrose;

import static com.gildedrose.ItemGildedRoseFactory.*;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;

class GoldenRuleTest {
  private static final int FIXED_SEED = 100;
  private static final int NUMBER_OF_RANDOM_ITEMS = 2000;
  private static final int MINIMUM = -50;
  private static final int MAXIMUM = 101;

  private final Random random = new Random(FIXED_SEED);

  private final List<String> itemNames =
      List.of(
          "+5 Dexterity Vest",
          "Aged Brie",
          "Elixir of the Mongoose",
          "Sulfuras, Hand of Ragnaros",
          "Backstage passes to a TAFKAL80ETC concert",
          "Conjured Mana Cake");

  @Test
  void should_generate_update_quality_output() {
    List<ItemGildedRose> items = generateRandomItems();

    GildedRose gildedRose = new GildedRose(items);
    gildedRose.updateQuality();

    Approvals.verify(generateStringRepresentation(items));
  }

  private List<ItemGildedRose> generateRandomItems() {
    return IntStream.range(0, NUMBER_OF_RANDOM_ITEMS)
        .mapToObj(index -> createItem(generateItemName(), generateSellIn(), generateQuality()))
        .collect(Collectors.toList());
  }

  private String generateItemName() {
    return itemNames.get(random.nextInt(itemNames.size()));
  }

  private int generateSellIn() {
    return randomNumber();
  }

  private int generateQuality() {
    return randomNumber();
  }

  private int randomNumber() {
    return MINIMUM + random.nextInt(MAXIMUM);
  }

  private String generateStringRepresentation(List<ItemGildedRose> items) {
    StringBuilder builder = new StringBuilder();
    for (ItemGildedRose item : items) {
      builder.append(item).append("\r");
    }
    return builder.toString();
  }
}
