package com.gildedrose;

class ItemFixture {

  private Item item;

  private ItemFixture() {}

  static class Builder {
    private String name;
    private int sellIn;
    private int quality;

    static Builder newItem() {
      return new Builder();
    }

    Builder withName(String name) {
      this.name = name;
      return this;
    }

    Builder withSellIn(int sellIn) {
      this.sellIn = sellIn;
      return this;
    }

    Builder withQuality(int quality) {
      this.quality = quality;
      return this;
    }

    ItemGildedRose build() {
      return ItemGildedRoseFactory.createItem(name, sellIn, quality);
    }
  }
}
