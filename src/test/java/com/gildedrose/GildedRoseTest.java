package com.gildedrose;

import static com.gildedrose.GildedRoseFixture.Builder.*;
import static com.gildedrose.ItemFixture.Builder.newItem;
import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class GildedRoseTest {

  GildedRoseFixture gildedRoseFixture;

  @Test
  void decrease_quality_with_one_after_one_days() {
    given(aGildedRose().with(newItem().withName("foo").withQuality(5).withSellIn(3)));

    whenUpdatingQuality();

    thenQualityIsEqualTo(4);
  }

  @Test
  void decrease_quality_with_two_after_two_days() {
    given(aGildedRose().with(newItem().withName("foo").withQuality(5).withSellIn(3)));

    whenUpdatingQuality();
    whenUpdatingQuality();

    thenQualityIsEqualTo(3);
  }

  @Test
  void decrease_selling_value_with_one_after_one_day() {
    given(aGildedRose().with(newItem().withName("foo").withQuality(3).withSellIn(5)));

    whenUpdatingQuality();

    thenSellInIsEqualTo(4);
  }

  @Test
  void decrease_selling_value_with_two_after_two_days() {
    given(aGildedRose().with(newItem().withName("foo").withQuality(3).withSellIn(5)));

    whenUpdatingQuality();
    whenUpdatingQuality();

    thenSellInIsEqualTo(3);
  }

  @Test
  void decrease_quality_twice_as_fast_after_expired_date() {
    given(aGildedRose().with(newItem().withName("foo").withQuality(3).withSellIn(0)));

    whenUpdatingQuality();

    thenQualityIsEqualTo(1);
  }

  @Test
  void quality_can_not_be_negative() {
    given(aGildedRose().with(newItem().withName("foo").withQuality(0).withSellIn(3)));

    whenUpdatingQuality();

    thenQualityIsEqualTo(0);
  }

  @Test
  void aged_brie_increase_quality_by_one_after_one_day() {
    given(aGildedRose().with(newItem().withName("Aged Brie").withQuality(5).withSellIn(3)));

    whenUpdatingQuality();

    thenQualityIsEqualTo(6);
  }

  @Test
  void aged_brie_increase_quality_by_two_after_two_days() {
    given(aGildedRose().with(newItem().withName("Aged Brie").withQuality(5).withSellIn(3)));

    whenUpdatingQuality();
    whenUpdatingQuality();

    thenQualityIsEqualTo(7);
  }

  @Test
  void quality_of_item_can_not_be_more_than_50() {
    given(aGildedRose().with(newItem().withName("Aged Brie").withQuality(50).withSellIn(3)));

    whenUpdatingQuality();

    thenQualityIsEqualTo(50);
  }

  @Test
  void sulfur_should_not_change() {
    given(
        aGildedRose()
            .with(newItem().withName("Sulfuras, Hand of Ragnaros").withQuality(5).withSellIn(3)));

    whenUpdatingQuality();

    thenQualityIsEqualTo(5);
    thenSellInIsEqualTo(3);
  }

  @Test
  void backstage_increase_quality_by_1_when_more_than_10_days_before_concert() {
    given(
        aGildedRose()
            .with(
                newItem()
                    .withName("Backstage passes to a TAFKAL80ETC concert")
                    .withQuality(5)
                    .withSellIn(15)));

    whenUpdatingQuality();

    thenQualityIsEqualTo(6);
  }

  @ParameterizedTest
  @CsvSource({"6,6", "7,7", "8,8", "9,9", "10,10"})
  void backstage_increase_quality_by_2_when_6_and_10_days_before_concert(int quality, int sellIn) {
    given(
        aGildedRose()
            .with(
                newItem()
                    .withName("Backstage passes to a TAFKAL80ETC concert")
                    .withQuality(quality)
                    .withSellIn(sellIn)));

    whenUpdatingQuality();

    thenQualityIsEqualTo(quality + 2);
  }

  @ParameterizedTest
  @CsvSource({"5,5", "4,4", "3,3", "2,2", "1,1"})
  void backstage_increase_quality_by_3_when_1_and_5_days_before_concert(int quality, int sellIn) {
    given(
        aGildedRose()
            .with(
                newItem()
                    .withName("Backstage passes to a TAFKAL80ETC concert")
                    .withQuality(quality)
                    .withSellIn(sellIn)));

    whenUpdatingQuality();

    thenQualityIsEqualTo(quality + 3);
  }

  @Test
  void back_stage_passes_quality_drops_to_zero_after_concert() {
    given(
        aGildedRose()
            .with(
                newItem()
                    .withName("Backstage passes to a TAFKAL80ETC concert")
                    .withQuality(5)
                    .withSellIn(0)));

    whenUpdatingQuality();

    thenQualityIsEqualTo(0);
  }

  @Test
  void conjured_degrade_in_2_quality_for_every_day() {
    given(aGildedRose().with(newItem().withName("Conjured").withQuality(5).withSellIn(3)));

    whenUpdatingQuality();

    thenQualityIsEqualTo(3);
  }

  @Test
  void conjured_degrade_4_in_quality_for_every_day_if_expired() {
    given(aGildedRose().with(newItem().withName("Conjured").withQuality(5).withSellIn(0)));

    whenUpdatingQuality();

    thenQualityIsEqualTo(1);
  }

  private void whenUpdatingQuality() {
    gildedRoseFixture.updateQuality();
  }

  private void given(GildedRoseFixture.Builder builder) {
    gildedRoseFixture = builder.build();
  }

  private void thenSellInIsEqualTo(int expected) {
    assertThat(gildedRoseFixture.getFirstItem().sellIn).isEqualTo(expected);
  }

  private void thenQualityIsEqualTo(int expected) {
    assertThat(gildedRoseFixture.getFirstItem().quality).isEqualTo(expected);
  }
}
