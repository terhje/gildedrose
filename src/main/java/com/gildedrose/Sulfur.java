package com.gildedrose;

public class Sulfur extends ItemGildedRose {
  public Sulfur(String name, int sellIn, int quality) {
    super(name, sellIn, quality);
  }

  @Override
  public void updateQuality() {
    // Should not update quality
  }
}
