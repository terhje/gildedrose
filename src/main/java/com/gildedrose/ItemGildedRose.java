package com.gildedrose;

abstract class ItemGildedRose extends Item {

  ItemGildedRose(String name, int sellIn, int quality) {
    super(name, sellIn, quality);
  }

  abstract void updateQuality();

  void decreaseSellIn() {
    sellIn--;
  }

  void increaseQuality() {
    if (quality < 50) {
      quality++;
    }
  }

  void decreaseQuality() {
    quality--;
  }
}
