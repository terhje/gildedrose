package com.gildedrose;

class ItemGildedRoseFactory {

  static ItemGildedRose createItem(String name, int sellIn, int quality) {
    return switch (name) {
      case "Aged Brie" -> new AgedBrie(name, sellIn, quality);
      case "Backstage passes to a TAFKAL80ETC concert" -> new Backstage(name, sellIn, quality);
      case "Sulfuras, Hand of Ragnaros" -> new Sulfur(name, sellIn, quality);
      case "Conjured" -> new Conjured(name, sellIn, quality);
      default -> new DefaultItem(name, sellIn, quality);
    };
  }
}
