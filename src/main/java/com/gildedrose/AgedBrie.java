package com.gildedrose;

public class AgedBrie extends ItemGildedRose {

  AgedBrie(String name, int sellIn, int quality) {
    super(name, sellIn, quality);
  }

  @Override
  public void updateQuality() {
    decreaseSellIn();
    increaseQuality();

    if (sellIn < 0) {
      increaseQuality();
    }
  }
}
