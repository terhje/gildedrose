package com.gildedrose;

public class DefaultItem extends ItemGildedRose {

  public DefaultItem(String name, int sellIn, int quality) {
    super(name, sellIn, quality);
  }

  @Override
  public void updateQuality() {
    decreaseSellIn();

    if (quality > 0) {
      decreaseQuality();
    }
    if (sellIn < 0 && quality > 0) {
      decreaseQuality();
    }
  }
}
