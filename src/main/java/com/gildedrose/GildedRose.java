package com.gildedrose;

import java.util.List;

class GildedRose {
  List<ItemGildedRose> items;

  public GildedRose(List<ItemGildedRose> items) {
    this.items = items;
  }

  public void updateQuality() {
    for (ItemGildedRose item : items) {
      item.updateQuality();
    }
  }
}
