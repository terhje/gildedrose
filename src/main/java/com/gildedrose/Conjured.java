package com.gildedrose;

public class Conjured extends ItemGildedRose {

  public Conjured(String name, int sellIn, int quality) {
    super(name, sellIn, quality);
  }

  @Override
  public void updateQuality() {
    decreaseSellIn();

    if (quality > 0) {
      decreaseQuality();
      decreaseQuality();
    }
    if (sellIn < 0 && quality > 0) {
      decreaseQuality();
      decreaseQuality();
    }
  }
}
