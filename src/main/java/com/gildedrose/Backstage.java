package com.gildedrose;

public class Backstage extends ItemGildedRose {
  public Backstage(String name, int sellIn, int quality) {
    super(name, sellIn, quality);
  }

  @Override
  public void updateQuality() {
    increaseQuality();

    if (sellIn < 11) {
      increaseQuality();
    }
    if (sellIn < 6) {
      increaseQuality();
    }
    decreaseSellIn();

    if (sellIn < 0) {
      quality = 0;
    }
  }
}
